//import logo from './logo.svg';
//import './App.css';
import React, { Component } from 'react';
import io from 'socket.io-client';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

class App extends Component {

  state = {
    isConnected:false,
    id:null,
    arr2:[ ],
    arr:[ ]
   
    //peeps:[]
    
  }
  socket = null

  componentWillMount(){

    this.socket = io('https://codi-server2.herokuapp.com');

    this.socket.on('connect', () => {
      this.setState({isConnected:true})
    
    })
   
    this.socket.on('pong!',(additionalStuff)=>{
      console.log('the server answered!',additionalStuff)
    })
    this.socket.on('youare',(answer)=>{
      this.setState({id:answer.id})
    })
    this.socket.on('disconnect', () => {
      this.setState({isConnected:false})
    })
    this.socket.on ('peeps',(peeps) => {
      this.setState({arr:peeps})
      //this.state.arr.join("\n") 
      //this.setState({nbr:peeps.length})
      //console.log(peeps +" "+ peeps.length)
     
 
    })
    this.socket.on('new connection',(answer)=>{
      this.state.arr.push(answer)
      this.setState({arr:this.state.arr})
    })
    this.socket.on('new disconnection',(answer)=>{
      this.state.arr.pop(answer)
      this.setState({arr:this.state.arr})
    })
    this.socket.on('next',(message_from_server)=>console.log(message_from_server))
    /** this will be useful way, way later **/
    this.socket.on('room', old_messages => {console.log(old_messages)
      // this.state.arr2.push(old_messages)
      // this.setState({arr2:this.state.arr2})
      this.setState({arr2:old_messages})
    })
      


  }
  // console.log(old_messages)
  componentWillUnmount(){
    this.socket.close()
    this.socket = null
  }
  
  render() {
    return (
      <div className="App">
        <div>status: {this.state.isConnected ? 'connected' : 'disconnected'}</div>
        <div>id: {this.state.id}</div>
        {/* <div>peeps: {this.state.arr}</div> */}
        <div>peeps: {this.state.arr.map((peepid, index) => {
        let peepsid = `${index+1} :\n ${peepid}`
        return `${peepsid} `
      })}</div>


        <div>arr length: {this.state.arr.length}</div>
        <button onClick={()=>this.socket.emit('ping!')}>ping</button>
        <button onClick={()=>this.socket.emit('whoami')}>Who am I?</button>
       <button onClick={()=>this.socket.emit('give me next')}>Give me next</button>
        <button onClick={()=>this.socket.emit('addition')}>addition</button>
        
        {/* <input onChange={()=>this.socket.emit("answer", 71 + 51 + 48)}/> */}
        {/* var message = {id:1, name:Christelle, text:" "}; */}
        {/* <input></input> */}

        <input id="usermessage" type="text"></input>
        <button
          onClick={() => {
            this.socket.emit("message", {
              text: document.getElementById("usermessage").value,
              id: this.state.id,
              name: "Christelle",
            });
          }}
        >
          Send
        </button>

        {/* <button onClick={()=>this.socket.emit('message', document.querySelector('input').value)}>Send</button> */}
        {/* <div>oldmsgs:{this.state.arr2}</div>  */}

        {/* <div>old: {this.state.arr2.map((p) => {
        let psid = p.text
        return psid
      })}</div> */}

               <div>{this.state.arr2.map(myData => {
                  return (
                    <div className='message'>
                      <div >
                        <span > {myData.name}:</span>
                        <span >{myData.date}</span>
                      </div>
                      <div className='content'>
                        <span className='text'>{myData.text} </span>
                      </div>
                    </div>
                  );
                })}</div>


      </div>
    );
  }
}

export default App;

